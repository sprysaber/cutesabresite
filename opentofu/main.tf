provider "vsphere" {
  user           = "user"
  password       = "passцord"
  vsphere_server = "192.168.0.86"

  allow_unverified_ssl = true
}

resource "vsphere_virtual_machine" "vm" {
  name             = "vm-s-001"
  resource_pool_id = "ResourcePool-resgroup-11"
  datastore_id     = "Datastore-datastore-14"
  num_cpus         = 2
  memory           = 4096
  guest_id         = "otherGuest"
  network_interface {
    network_id   = "key-vim.host.VirtualSwitch-vSwitch0"
    adapter_type = "vmxnet3"
  }
  disk {
    label            = "disk0"
    size             = 20
    eagerly_scrub    = false
    thin_provisioned = true
  }
}
