#!/bin/bash

echo "Running Bandit for Python code analysis..."
bandit -r .

echo "Running Brakeman for Ruby on Rails code analysis..."
brakeman .

echo "Running ESLint for JavaScript code analysis..."
eslint .

echo "Combining reports into gl-sast-report.json..."
jq -s '.[0] * .[1] * .[2]' bandit-output.json brakeman-output.json eslint-output.json > gl-sast-report.json

echo "SAST analysis completed."
