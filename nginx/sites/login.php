<?php
$host = '192.168.0.139';
$dbname = 'sprysabresite';
$user = 'stardust';
$password = 'password12345';

// Соединяемся с базой данных
$conn = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$response = ['status' => 'success', 'message' => 'Авторизация успешна!'];

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Получаем данные из формы
    $username = $_POST['username'];
    $password = $_POST['password'];

    try {
        // Проверяем наличие пользователя в базе данных
        $stmt = $conn->prepare("SELECT password FROM site_users WHERE username = :username");
        $stmt->bindParam(':username', $username);
        $stmt->execute();
        $hashed_password = $stmt->fetchColumn();

        if ($hashed_password && password_verify($password, $hashed_password)) {
            $response['redirect'] = 'authuser.html'; // редирект при успешной авторизации
        } else {
            $response = ['status' => 'error', 'message' => 'Неверное имя пользователя или пароль'];
        }
    } catch (PDOException $e) {
        $response = ['status' => 'error', 'message' => "Ошибка: " . $e->getMessage()];
    }
}

$conn = null;
echo json_encode($response);
?>
