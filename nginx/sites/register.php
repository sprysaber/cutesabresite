<?php
$host = '192.168.0.139';
$dbname = 'sprysabresite';
$user = 'stardust';
$password = 'password12345';

// Соединяемся с базой данных
$conn = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$response = ['status' => 'success', 'message' => 'Регистрация успешна!'];

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Получаем данные из формы
    $username = $_POST['username'];
    $password = password_hash($_POST['password'], PASSWORD_DEFAULT); // хэшируем пароль
    $email = $_POST['email'];

    try {
        // Проверяем уникальность username и email
        $stmt = $conn->prepare("SELECT COUNT(*) FROM site_users WHERE username = :username OR email = :email");
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':email', $email);
        $stmt->execute();
        $count = $stmt->fetchColumn();

        if ($count > 0) {
            $response = ['status' => 'error', 'message' => 'Такое имя пользователя или Email уже существуют'];
        } else {
            // Вставляем данные в таблицу
            $sql = "INSERT INTO Site_users (username, password, email)
                    VALUES (:username, :password, :email)";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':username', $username);
            $stmt->bindParam(':password', $password);
            $stmt->bindParam(':email', $email);
            $stmt->execute();
            $response['redirect'] = 'authuser.html';
        }
    } catch (PDOException $e) {
        $response = ['status' => 'error', 'message' => "Ошибка: " . $e->getMessage()];
    }
}

$conn = null;
echo json_encode($response);
?>
