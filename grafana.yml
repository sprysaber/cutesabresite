---
- name: Установка Grafana
  hosts: grafana
  become: yes
  vars:
    prometheus_url: "http://192.168.0.141:9090"
    grafana_version: "11.1.0"

  tasks:
    - name: Создание пользователя grafana
      ansible.builtin.user:
        name: grafana
        createhome: no
        shell: /usr/sbin/nologin

    - name: Создаем папку для распаковки архива
      file:
        path: "/usr/share/grafana-{{ grafana_version }}"
        state: directory
        owner: root
        group: root
        mode: '0755'

    - name: Скачиваем архив с  Grafana
      get_url:
        url: "https://dl.grafana.com/oss/release/grafana-{{ grafana_version }}.linux-amd64.tar.gz"
        dest: "/tmp/grafana-{{ grafana_version }}.linux-amd64.tar.gz"

    - name: Распаковываем архив Grafana
      ansible.builtin.unarchive:
        src: "/tmp/grafana-{{ grafana_version }}.linux-amd64.tar.gz"
        dest: "/usr/share/grafana-{{ grafana_version }}"
        remote_src: yes

    - name: Создаем список файлов для вывода
      command: ls -l /usr/share/grafana-{{ grafana_version }}/grafana-v{{ grafana_version }}
      register: grafana_files

    - name: Выводим что распаковали
      debug:
        var: grafana_files

    - name: Создаем папки для  Grafana
      file:
        path: "{{ item }}"
        state: directory
        owner: grafana
        group: grafana
        mode: '0755'
      with_items:
        - /etc/grafana
        - /var/lib/grafana
        - /var/log/grafana

    - name: Копируем файлы в /usr/share/grafana
      copy:
        src: "/usr/share/grafana-{{ grafana_version }}/grafana-v{{ grafana_version }}/"
        dest: "/usr/share/grafana/"
        owner: grafana
        group: grafana
        mode: '0755'
        remote_src: yes

    - name: Устанавливаем права на grafana-server*
      file:
        path: /usr/share/grafana/bin/grafana-server
        mode: '0755'

    - name: Добавляем переменную PATH
      lineinfile:
        path: /etc/profile
        line: "export PATH=/usr/share/grafana/bin:$PATH"
        state: present

    - name: Создаем папки для конфигов Prometheus
      file:
        path: /etc/grafana/provisioning/datasources
        state: directory
        owner: grafana
        group: grafana
        mode: '0755'

    - name: Создаем конфиг datasources/prometheus
      copy:
        dest: /etc/grafana/provisioning/datasources/prometheus.yaml
        content: |
          apiVersion: 1
          datasources:
            - name: Prometheus
              type: prometheus
              url: {{ prometheus_url }}

    - name: Создаем файл службы Grafana
      copy:
        dest: /etc/systemd/system/grafana-server.service
        content: |
          [Unit]
          Description=Grafana instance
          After=network.target

          [Service]
          ExecStart=/usr/share/grafana/bin/grafana-server web
          User=grafana
          Group=grafana
          WorkingDirectory=/usr/share/grafana
          Restart=always
          Environment=GF_PATHS_DATA=/var/lib/grafana
          Environment=GF_PATHS_LOGS=/var/log/grafana
          Environment=GF_PATHS_PLUGINS=/var/lib/grafana/plugins
          Environment=GF_PATHS_PROVISIONING=/etc/grafana/provisioning

          [Install]
          WantedBy=multi-user.target

    - name: Перезагружаем systemd daemon
      systemd:
        daemon_reload: yes

    - name: Включаем службу grafana-service
      systemd:
        name: grafana-server
        enabled: yes
        state: started

    - name: Проверяем работоспособность grafana-service
      systemd:
        name: grafana-server
        state: started
        enabled: yes

    - name: Проверяем состояние active
      command: systemctl is-active grafana-server
      register: grafana_status
      failed_when: grafana_status.stdout != "active"
      ignore_errors: yes

    - name:  Выводим статус grafana-service
      debug:
        msg: "Grafana service status: {{ grafana_status.stdout }}"
