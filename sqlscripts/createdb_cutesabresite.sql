-- Создание таблиц согласно модели базы данных
\c sprysabresite;

CREATE TABLE site_users (
    user_id SERIAL PRIMARY KEY,
    username VARCHAR(50) NOT NULL UNIQUE,
    password VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE,
    date_creation TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP AT TIME ZONE 'UTC+3',
    guid_user UUID NOT NULL DEFAULT gen_random_uuid()
);

CREATE TABLE event (
    event_id SERIAL PRIMARY KEY,
    event_name VARCHAR(100) NOT NULL,
    date DATE NOT NULL,
    location VARCHAR(100) NOT NULL,
    price DECIMAL(10, 2) NOT NULL,
    category_id INT REFERENCES Category(category_id)
);

CREATE TABLE ticket (
    ticket_id SERIAL PRIMARY KEY,
    user_id INT REFERENCES site_users(user_id),
    guid_user UUID REFERENCES User(guid_user),
    event_id INT REFERENCES Event(event_id),
    status VARCHAR(20) NOT NULL
);

CREATE TABLE profile (
    profile_id SERIAL PRIMARY KEY,
    user_id INT REFERENCES site_users(user_id),
    bio TEXT,
    interests TEXT,
    rating DECIMAL(3, 2)
);

CREATE TABLE actions (
    action_id SERIAL PRIMARY KEY,
    user_id INT REFERENCES site_users(user_id),
    action_type VARCHAR(100),
    timestamp TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP AT TIME ZONE 'UTC+3'
);

CREATE TABLE reviews (
    review_id SERIAL PRIMARY KEY,
    user_id INT REFERENCES site_users(user_id),
    event_id INT REFERENCES Event(event_id),
    rating DECIMAL(2, 1),
    comment TEXT
);

CREATE TABLE category (
    category_id SERIAL PRIMARY KEY,
    category_name VARCHAR(100) NOT NULL
);

CREATE TABLE friends (
    friend_id SERIAL PRIMARY KEY,
    user_id INT REFERENCES site_users(user_id),
    friend_user_id INT REFERENCES site_users(user_id)
);

CREATE TABLE notifications (
    notification_id SERIAL PRIMARY KEY,
    user_id INT REFERENCES site_users(user_id),
    message TEXT,
    timestamp TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP AT TIME ZONE 'UTC+3'
);

CREATE TABLE transactions (
    transaction_id SERIAL PRIMARY KEY,
    user_id INT REFERENCES site_users(user_id),
    amount DECIMAL(10, 2),
    payment_method VARCHAR(50),
    timestamp TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP AT TIME ZONE 'UTC+3'
);

CREATE TABLE messages (
    message_id SERIAL PRIMARY KEY,
    sender_id INT REFERENCES site_users(user_id),
    receiver_id INT REFERENCES site_users(user_id),
    content TEXT,
    timestamp TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP AT TIME ZONE 'UTC+3'
);

CREATE TABLE invitations (
    invitation_id SERIAL PRIMARY KEY,
    sender_id INT REFERENCES site_users(user_id),
    receiver_id INT REFERENCES site_users(user_id),
    event_id INT REFERENCES Event(event_id),
    status VARCHAR(20),
    timestamp TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP AT TIME ZONE 'UTC+3'
);
