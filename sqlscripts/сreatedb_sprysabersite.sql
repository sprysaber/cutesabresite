-- Подключение к серверу от имени пользователя postgres и создание базы данных
\c postgres
CREATE DATABASE sprysabresite;

-- Подключение к новой базе данных
\c sprysabresite

-- Создание таблицы registred_users
CREATE TABLE registred_users (
    name VARCHAR(255),
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    email_address VARCHAR(255),
    date_creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
