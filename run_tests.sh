#!/bin/bash

# Проверка наличия PHPUnit
if ! command -v phpunit &> /dev/null
then
    echo "PHPUnit не найден. Убедитесь, что он установлен."
    exit 1
fi

# Запуск тестов с PHPUnit
echo "Запуск тестов с PHPUnit..."
phpunit --configuration phpunit.xml

# Проверка успешности выполнения тестов
if [ $? -eq 0 ]; then
    echo "Все тесты пройдены успешно."
else
    echo "Некоторые тесты не пройдены."
    exit 1
fi
